package br.net.morais;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
public class QuickStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuickStarterApplication.class, args);
	}

}

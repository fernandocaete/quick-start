package br.net.morais.ui.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("users")
public class UserController {

	@GetMapping()
	public String getUser(@RequestParam(name = "page") int page, @RequestParam(name = "limit") int limit) {
		return "get user as called user with page = " + page + " and limit = " + limit;
	}
	
	@GetMapping(path = "/{userId}")
	public String getUser(@PathVariable String userId) {
		return "get user as called user id = " + userId;
	}
	
	@PostMapping
	public String createUser() {
		return "create user as called";
	}
	
	@PutMapping
	public String updateUser() {
		return "update user as called";
	}
	
	@DeleteMapping
	public String deleteUser() {
		return "delete user as called";
	}
}
